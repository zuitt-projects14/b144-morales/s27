const http = require("http");

//sa labas ng function ang mock database

let directory = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"
}
]




http.createServer((request, response) => {
//Route for returnong all items upon receiving a GET request
if(request.url == "/users" && request.method == "GET"){

	response.writeHead(200, {"Content-Type": "application/json"})
	response.write(JSON.stringify(directory));


	//when info is sent to the client, it is sent in the format of a stringified format
	response.end()
}

if(request.url == "/users" && request.method == "POST"){
	//DEclare and Initialize "requestBody" variable to an empty string.
	//This will act as a placeholder for the resources/data to be created later on
	//ex. Jane Doe.
	let requestBody = ""
	//Stream = it is a sequence of data.
	//1. Data is received from the client and is processed in the keyword "data" stream called "data" the code below will be triggered.
	//data step - this reads the "data" stream and process it as the request body.
	request.on("data", function(data){
		//Assign the data retrieved from the  data stream to requestBody
		requestBody += data;
})

//2. end step - only runs after the request has completely been sent.
//Mag rurun lang ito pag nakuha na ang data.
//end response once the data is already processed.
request.on("end", function(){
//Check if at this point the requestBody is of data type STRING.
//We need this to be of data type JSON to access its properties.
console.log(typeof requestBody)
//converts the string requestBody to JSON

requestBody = JSON.parse(requestBody)

//Create a new object representing the new mock database record.
let newUser = {
	"name": requestBody.name,
	"email": requestBody.email
}
//add the mock data base to postman
//Add the new User into the mock database
directory.push(newUser)
console.log(directory);


response.writeHead(200, {"Content-Type": "application/json"})
response.write(JSON.stringify(newUser));
response.end()


})


}

/*<body>
firstName:
<input type= "text" />Jane
<button>
</body>*/
//after ma click ng button jane ay papasok na sa requestBody


}).listen(4000)


console.log("Server running at localhost:4000");



